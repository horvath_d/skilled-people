'use strict';

const List = require('../components/List');
const ListStore = require('../stores/ListStore');

const Element = React.createClass({

  componentDidMount: function() {
    if (this.props.getHeight) {
      this.props.getHeight($(ReactDOM.findDOMNode(this.refs.listElement)).height());
    }
  },

  render: function() {
    return <div ref='listElement' className='list-element'>{this.props.name}</div>;
  },
});

const PlaceHolder = React.createClass({

  render: function() {
    let height = this.props.height;
    return <div className='placeholder' style={{ height: height }}></div>;
  },
});

module.exports = React.createClass({
  mixins: [
    Reflux.listenTo(ListStore, '_onListUpdate'),
  ],

  $listContainer: null,
  resolution: 200,

  getInitialState: function() {
    return {
      list: ListStore.getListData(),
      isListDataFiltered: false,
      elementHeight: 0,
      scrollHeight: 0,
      startIndex: 0,
      endIndex: this.resolution * 3,
      startPlaceHolderHeight: 0,
      endPlaceHolderHeight: 0,
      isLoading: true,
    };
  },

  _onListUpdate: function() {
    this.setState({
      list: ListStore.getListData(),
      isListDataFiltered: ListStore.isListDataFiltered,
      listDataLength: ListStore.getListDataLength(),
      isLoading: false,
    });
    this._gapCalculator();
  },

  sortAscByLastName: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.sortAscByLastName();
  },

  sortDescByLastName: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.sortDescByLastName();
  },

  sortAscByBirthDate: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.sortAscByBirthDate();
  },

  sortDescByBirthDate: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.sortDescByBirthDate();
  },

  filterBySkill: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.filterBySkill({ skill: 'Medicine' });
  },

  filterByGender: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.filterByGender({ gender: 'male' });
  },

  filterYoungSkilledInSport: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.filterYoungSkilledInSport();
  },

  filterFemaleSkilledInScience: function() {
    this.setState({ isLoading: true });
    this.$listContainer.scrollTop(0);
    ListStore.filterFemaleSkilledInScience();
  },

  componentDidMount: function() {
    this.$listContainer = $(ReactDOM.findDOMNode(this.refs.list));
  },

  componentDidUpdate: function(prevProps, prevStates) {
    if (prevStates.elementHeight !== this.state.elementHeight) {
      this._gapCalculator();
    }

    if (prevStates.startIndex !== this.state.startIndex || prevStates.endIndex !== this.state.endIndex) {
      this._setListDataSlice();
    }
  },

  _getElementHeight: function(height) {
    this.setState({
      elementHeight: height,
    });
  },

  _onScroll: function() {
    let elementHeight = this.state.elementHeight;
    let startBoundary = this.state.startIndex * elementHeight;
    let endBoundary = this.state.endIndex * elementHeight;
    let scrollPosition = this.$listContainer.get(0).scrollTop;
    let scrollHeight = this.$listContainer.get(0).scrollHeight;

    if (scrollPosition < this.resolution * elementHeight) {
      if (!this.state.isStartSet) {
        this.setState({
          startIndex: 0,
          endIndex: this.resolution * 3,
          isStartSet: true,
          isEndSet: false,
        });
      }

      return;
    }

    if (scrollHeight - scrollPosition < this.resolution * elementHeight) {
      if (!this.state.isEndSet) {
        this.setState({
          startIndex: this.state.listDataLength - this.resolution * 3,
          endIndex: this.state.listDataLength,
          isEndSet: true,
          isStartSet: false,
        });
      }

      return;
    }

    if ((scrollPosition - startBoundary) < this.resolution * elementHeight || (endBoundary - scrollPosition) < this.resolution * elementHeight) {
      this.setState({
        startIndex: (scrollPosition / elementHeight) - ((scrollPosition / elementHeight) % this.resolution) - this.resolution,
        endIndex: (scrollPosition / elementHeight) - ((scrollPosition / elementHeight) % this.resolution)  + this.resolution * 2,
        isStartSet: false,
        isEndSet: false,
      });
    }
  },

  _setListDataSlice: function() {
    this.state.isListDataFiltered
    ? ListStore.getFilteredData({ from:this.state.startIndex, until: this.state.endIndex })
    : ListStore.getData({ from:this.state.startIndex, until: this.state.endIndex });
  },

  _gapCalculator: function() {

    let calculatedScrollHeight = this.state.listDataLength * this.state.elementHeight;

    this.setState({
      startPlaceHolderHeight: this.state.elementHeight * this.state.startIndex,
      endPlaceHolderHeight: calculatedScrollHeight - (this.state.endIndex * this.state.elementHeight),
    });
  },

  render: function() {

    let list = this.state.list || [];
    let startPlaceHolderHeight = this.state.startPlaceHolderHeight / 10;
    let endPlaceHolderHeight = this.state.endPlaceHolderHeight / 10;

    let startPlaceHolders = [];
    let endPlaceHolders = [];

    if (!this.state.isLoading) {
      for (var i = 0; i < 10; i++) {
        startPlaceHolders.push(<PlaceHolder key={i} height={startPlaceHolderHeight} />);
        endPlaceHolders.push(<PlaceHolder key={i} height={endPlaceHolderHeight} />);
      }
    } else {
      startPlaceHolders = [];
      endPlaceHolders = [];
    }

    let renderList = this.state.isLoading ? ['LOADING...'] : list.map((e, i) => {
      let value = '||birthDate: ' + e.birthDate + ' ||firstName: ' + e.firstName + ' ||lastName: ' + e.lastName + ' ||sex: ' + e.sex + ' ||skills: ' + e.skills;
      if (i === 0) {
        return <Element key={i} getHeight={this._getElementHeight} name={value}/>;
      }

      return <Element key={i} name={value}/>;
    });

    return (
      <div className='container'>
        <div className='button-container row text-center'>
          <div className='col-md-12'>
            <div className='row'>
              <div className='col-md-12'>
                <button className='btn btn-default btn-sm' onClick={this.sortAscByLastName}>sortAscByLastName</button>
                <button className='btn btn-default btn-sm' onClick={this.sortDescByLastName}>sortDescByLastName</button>
                <button className='btn btn-default btn-sm' onClick={this.sortAscByBirthDate}>sortAscByBirthDate</button>
                <button className='btn btn-default btn-sm' onClick={this.sortDescByBirthDate}>sortDescByBirthDate</button>
              </div>
            </div>
            <div className='row'>
              <div className='col-md-12'>
                <button className='btn btn-default btn-sm' onClick={this.filterBySkill}>filterBySkill</button>
                <button className='btn btn-default btn-sm' onClick={this.filterByGender}>filterByGender</button>
                <button className='btn btn-default btn-sm' onClick={this.filterYoungSkilledInSport}>filterYoungSkilledInSport</button>
                <button className='btn btn-default btn-sm' onClick={this.filterFemaleSkilledInScience}>filterFemaleSkilledInScience</button>
              </div>
            </div>
          </div>
          <div className='container col-md-12 col-xs-12'>
            <List visibleElements={100} scrollEvent={this._onScroll} ref='list'>
              {startPlaceHolders}
              {renderList}
              {endPlaceHolders}
            </List>
          </div>
        </div>
      </div>
    );
  },
});
