'use strict';

const Reflux = require('reflux');
const adl = require('../util/adl');

const ListStore = Reflux.createStore({

  listData: null,
  listDataLength: 0,
  isListDataFiltered: false,

  init: function() {
    this.sortAscByLastName();
  },

  getListData: function() {
    return this.listData;
  },

  getListDataLength: function() {
    return this.listDataLength;
  },

  // opt could be { from: [Number], until: [Number] } in any case
  sortAscByLastName: function(opt) {
    adl.getSocketData('sortAscByLastName', opt);
  },

  sortDescByLastName: function(opt) {
    adl.getSocketData('sortDescByLastName', opt);
  },

  sortAscByBirthDate: function(opt) {
    adl.getSocketData('sortAscByBirthDate', opt);
  },

  sortDescByBirthDate: function(opt) {
    adl.getSocketData('sortDescByBirthDate', opt);
  },

  // { skill: 'Science' }
  filterBySkill: function(opt) {
    adl.getSocketData('filterBySkill', opt);
  },

  // { gender: 'male' || 'female' }
  filterByGender: function(opt) {
    adl.getSocketData('filterByGender', opt);
  },

  filterYoungSkilledInSport: function(opt) {
    adl.getSocketData('filterYoungSkilledInSport', opt);
  },

  filterFemaleSkilledInScience: function(opt) {
    adl.getSocketData('filterFemaleSkilledInScience', opt);
  },

  getData: function(opt) {
    adl.getSocketData('getData', opt);
  },

  getFilteredData: function(opt) {
    adl.getSocketData('getFilteredData', opt);
  },
});

require('../util/socketCallbacks')(adl.socket, ListStore);

module.exports = ListStore;
