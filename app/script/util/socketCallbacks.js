'use strict ';

module.exports = function(socket, store) {
  socket.on('sortAscByLastName', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = false;
    store.trigger();
  });

  socket.on('sortDescByLastName', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = false;
    store.trigger();
  });

  socket.on('sortAscByBirthDate', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = false;
    store.trigger();
  });

  socket.on('sortDescByBirthDate', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = false;
    store.trigger();
  });

  socket.on('filterBySkill', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = true;
    store.trigger();
  });

  socket.on('filterByGender', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = true;
    store.trigger();
  });

  socket.on('filterYoungSkilledInSport', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = true;
    store.trigger();
  });

  socket.on('filterFemaleSkilledInScience', function(data) {
    store.listData = data.data;
    store.listDataLength = data.length;
    store.isListDataFiltered = true;
    store.trigger();
  });

  socket.on('getData', function(data) {
    if (store.isListDataFiltered) {
      console.error(`Data is not initialized correctly, data is filtered, will give back the last sorted state of data. store.isListDataFiltered: ${store.isListDataFiltered}`);
    }

    store.listData = data.data;
    store.listDataLength = data.length;
    store.trigger();
  });

  socket.on('getFilteredData', function(data) {
    if (!store.isListDataFiltered) {
      console.error(`Data is not initialized correctly, data is NOT filtered, will give back the last filtered state of data. store.isListDataFiltered: ${store.isListDataFiltered}`);
    }

    store.listData = data.data;
    store.listDataLength = data.length;
    store.trigger();
  });
};
