'use strict';

function Adl() {};

Adl.prototype.socket = io('http://localhost:3000');

Adl.prototype.post = function(options) {
  var options = options || {};
  return new Promise((resolve, reject) => {
    $.ajax({
      type: options.type,
      url: options.url,
      data: options.data,
      dataType: options.dataType,
      beforeSend: function(request) {
        request.setRequestHeader('Authorization', options.token);
      },

      success: function(data) {
        resolve(data);
      },

      error: function(err) {
        reject(err);
      },

    });
  });
};

Adl.prototype.getSocketData = function(event, data = {}) {
  data.from ? data.from : data.from = 0;
  data.until ? data.until : data.until = 600;
  this.socket.emit(event, data);
};

module.exports = new Adl();
