'use strict';

module.exports = React.createClass({

  getInitialState: function() {
    return {
      elements: this.props.elements || [],
    };
  },

  render: function() {

    let elements = this.props.children;

    return (
      <div className='list-container' onScroll={this.props.scrollEvent} >
        {elements.length > 0 &&
          elements}
      </div>);
  },
});
