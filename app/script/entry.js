(function() {
  var _ = require('./thirdparty/underscore.js');
  var React = require('react');
  var ReactDOM = require('react-dom');
  var adl = require('./util/adl.js');
  var Reflux = require('reflux');

  //Needed for React Developer Tools
  window.React = React;
  window.ReactDOM = ReactDOM;
  window.Reflux = Reflux;
  window._ = _;
  window.adl = adl;

  var Root = require('./root/Root.jsx');

  ReactDOM.render(<Root />, document.getElementById('mountPoint'));
})();
