'use strict';

const fileReader = require('./mockdata.js');

/*
o rendezés vezetéknév szerint
o rendezés életkor szerint
o szűrés a tehetség területére
o a 25 évnél nem idősebb emberek kiemelése, akik tehetségesek a “Sport” területén
o azon nők kiemelése, akik tehetségesek a “Tudományok” területén
*/
let _loadedData;
let _filteredData;

fileReader.readPackage().then((file) => {
  _loadedData = file;

  // sortASC('birthDate');
  // sortDESC('email');
  // console.log(_loadedData.slice(0, 10));
  // console.log(filterByGender('female').length);
  // console.log(filterBySkill('Medicine').length);
  // console.log(filterYoungSkilledInSport().slice(0, 25));
  // console.log(filterFemaleSkilledInScience().slice(0, 25));
}).catch((err) => {
  console.log(err);
});

// Helper function, not exported
function _mapF(arr, fn) {
  let retVal = [];

  // for (let i = 0; i < 1000; i++) {
  for (let i = 0; i < arr.length; i++) {
    fn(arr[i]) ? retVal.push(fn(arr[i])) : '';
  }

  return retVal;
};

function getData() {
  return new Promise((resolve, reject) => {
    resolve({ data: _loadedData, length: _loadedData.length });
  });
};

function getFilteredData() {
  return new Promise((resolve, reject) => {
    resolve({ data: _filteredData, length: _filteredData.length });
  });
};

function sortASC(by) {
  return new Promise((resolve, reject) => {
    _loadedData.sort(function(a, b) {
      if (a[by] < b[by]) {
        return -1;
      }

      if (a[by] > b[by]) {
        return 1;
      }

      return 0;
    });

    resolve({ data: _loadedData, length: _loadedData.length });
  });
};

function sortDESC(by) {
  return new Promise((resolve, reject) => {
    _loadedData.sort(function(a, b) {
      if (a[by] < b[by]) {
        return 1;
      }

      if (a[by] > b[by]) {
        return -1;
      }

      return 0;
    });

    resolve({ data: _loadedData, length: _loadedData.length });
  });
};

function filterBySkill(skill) {
  return new Promise((resolve, reject) => {
    _filteredData = _mapF(_loadedData, function(e) {
      if (e.skills.indexOf(skill) > 0) {
        return e;
      }
    });

    resolve({ data: _filteredData, length: _filteredData.length });
  });
};

function filterByGender(gender) {
  return new Promise((resolve, reject) => {
    _filteredData = _mapF(_loadedData, function(e) {
      if (e.sex === gender) {
        return e;
      }
    });

    resolve({ data: _filteredData, length: _filteredData.length });
  });
};

function filterYoungSkilledInSport() {
  return new Promise((resolve, reject) => {
    _filteredData = _mapF(_loadedData, function(e) {
      let jsDate = new Date(e.birthDate);
      let ageDifMs = Date.now() - jsDate.getTime();
      let ageDate = new Date(ageDifMs);
      if (e.skills.indexOf('Sport') > 0 &&  Math.abs(ageDate.getFullYear() - 1970) < 25) {
        return e;
      }
    });

    resolve({ data: _filteredData, length: _filteredData.length });
  });
};

function filterFemaleSkilledInScience() {
  return new Promise((resolve, reject) => {
    _filteredData =  _mapF(_loadedData, function(e) {
      if (e.sex === 'female' && e.skills.indexOf('Science') > 0) {
        return e;
      }
    });

    resolve({ data: _filteredData, length: _filteredData.length });
  });
};

module.exports = {
  sortASC,
  sortDESC,
  filterBySkill,
  filterByGender,
  filterYoungSkilledInSport,
  filterFemaleSkilledInScience,
  getData,
  getFilteredData,
};
