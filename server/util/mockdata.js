'use strict';

const fs = require('fs');

function readPackage(callback) {

  callback = callback || function() {};

  return new Promise((resolve, reject) => {
    // fs.readFile('server/mockdata/random_data.json', (err, data) => {
    fs.readFile('server/mockdata/random_data.json', (err, data) => {
      if (err) {
        reject(err);
        return callback(err);
      }

      let jsonFile;
      try {
        jsonFile = JSON.parse(data);
      } catch (e) {
        jsonFile = null;
      }

      resolve(jsonFile);
      return callback(null, jsonFile);
    });
  });
}

module.exports.readPackage = readPackage;
