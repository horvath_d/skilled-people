'use strict';

const express = require('express');
const router = express.Router();
const dh = require('../util/datahelper.js');

/*
o rendezés vezetéknév szerint
o rendezés életkor szerint
o szűrés a tehetség területére
o a 25 évnél nem idősebb emberek kiemelése, akik tehetségesek a “Sport” területén
o azon nők kiemelése, akik tehetségesek a “Tudományok” területén
*/

router.get('/', function(req, res, next) {
  res.status(400).send('Bad Request');
});

// LAST NAME SORT
router.get('/sortascbylastname', function(req, res, next) {
  dh.sortASC('lastName');
  res.send('Bad Request');
});

router.get('/sortdescbylastname', function(req, res, next) {
  dh.sortDESC('lastName');
  res.send('Bad Request');
});

// BIRTH DATE SORT
router.get('/sortascbybirthDate', function(req, res, next) {
  dh.sortASC('birthDate');
  res.send('Bad Request');
});

router.get('/sortdescbybirthDate', function(req, res, next) {
  dh.sortDESC('birthDate');
  res.send('Bad Request');
});

// SKILLS FILTER
router.get('/filterbyskill', function(req, res, next) {
  res.send('Bad Request');
});

// GENDER FILTER
router.get('/filterbygender', function(req, res, next) {
  res.send('Bad Request');
});

// YOUNGER THAN 25, SKILLED IN SPORT FILTER
router.get('/filteryoungskilledinsport', function(req, res, next) {
  res.send('Bad Request');
});

// FEMALE SKILLED IN SCIENCE FILTER
router.get('/filterfemaleskilledinscience', function(req, res, next) {
  res.send('Bad Request');
});

router.post('/post', function(req, res, next) {
  if (req.get('Authorization') === 'whatever') {
    req.accepts('application/json');
    res.format({
      json: function() {
        res.status(200).json({"valid": "true"});
      },

      default: function() {
        // log the request and respond with 406
        res.status(406).send('Not Acceptable');
      },
    });
  } else {
    res.status(400).json({valid: 'false'});
  }
});

module.exports = router;
