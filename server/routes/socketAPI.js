'use strict';

const dh = require('../util/datahelper.js');

let ioServer = null;

module.exports.createIOserver = function(server) {
  if (ioServer || !server) {
    return;
  }

  ioServer = require('socket.io')(server);

  ioServer.on('connection', function(socket) {
    socket.on('sortAscByLastName', function(opt) {
      dh.sortASC('lastName').then((data) => {
        socket.emit('sortAscByLastName', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on sortAscByLastName, data handler callback in socket API', err);
      });
    });

    socket.on('sortDescByLastName', function(opt) {
      dh.sortDESC('lastName').then((data) => {
        socket.emit('sortDescByLastName', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on sortDescByLastName, data handler callback in socket API', err);
      });
    });

    socket.on('sortAscByBirthDate', function(opt) {
      dh.sortASC('birthDate').then((data) => {
        socket.emit('sortAscByBirthDate', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on sortAscByBirthDate, data handler callback in socket API', err);
      });
    });

    socket.on('sortDescByBirthDate', function(opt) {
      dh.sortDESC('birthDate').then((data) => {
        socket.emit('sortDescByBirthDate', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on sortDescByBirthDate, data handler callback in socket API', err);
      });
    });

    socket.on('filterBySkill', function(opt) {
      dh.filterBySkill(opt.skill).then((data) => {
        socket.emit('filterBySkill', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on filterBySkill, data handler callback in socket API', err);
      });
    });

    socket.on('filterByGender', function(opt) {
      dh.filterByGender(opt.gender).then((data) => {
        socket.emit('filterByGender', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on filterByGender, data handler callback in socket API', err);
      });
    });

    socket.on('filterYoungSkilledInSport', function(opt) {
      dh.filterYoungSkilledInSport().then((data) => {
        socket.emit('filterYoungSkilledInSport', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on filterYoungSkilledInSport, data handler callback in socket API', err);
      });
    });

    socket.on('filterFemaleSkilledInScience', function(opt) {
      dh.filterFemaleSkilledInScience().then((data) => {
        socket.emit('filterFemaleSkilledInScience', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on filterFemaleSkilledInScience, data handler callback in socket API', err);
      });
    });

    socket.on('getData', function(opt) {
      dh.getData().then((data) => {
        socket.emit('getData', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on getData, data handler callback in socket API', err);
      });
    });

    socket.on('getFilteredData', function(opt) {
      dh.getFilteredData().then((data) => {
        socket.emit('getFilteredData', { data: data.data.slice(opt.from, opt.until), length: data.length });
      }).catch((err) => {
        console.log('on getFilteredData, data handler callback in socket API', err);
      });
    });
  });
};
