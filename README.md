In order to start the project please do the following steps:

1. npm i gulp -g
2. npm i nodemon -g
3. npm install
4. gulp


...

On the backend side there is an express app() which provides the possibility to easily create services and handle templating engines and serve public files.
On Front-End side there is an React.js app, which is bundled by webpack. The webpack and how the third parties are pulled in the project is very rudimentary, but please bear in mind I've focused on other areas.

The original idea was to put the generated profiles into a Mongo database what would have been located on my private VPS. This solution demanded too much resource and configuration, such as CORS, handling security, or setup the Mongo database on the linux server. I decided to switch to a more simple solution, which is to only bother with a single JSON file.

The file called random_data.json was generated by python code. I involved one of my mechanical engineer friend who was eager to solve a simple problem by writing script in python for learning purposes, therefore I decided to ask him and let him contribute on the trial task. Meantime I could take care of other side of the project like structure folder, development pipeline, third parties, etc.

The server side is a simple Express.js application listening on port 3000. The application uses SWIG render engine, I switched that because I don't like the syntax of Jade which is the default templating engine of Express.js. Basically I haven't utilized the SWIG render engine power, because I wanted to leverage  
At the beginning I planned to use REST and communicate through HTTP verbs, but finally I decided to move the whole communication to SocketIO. Maybe the interface are not following best practices, but I have no previous experience with SocketIO. I outsourced the socket interface on the server side into socketAPI.js which is basically responsible for listening events on an opened connection asynchronously load the data and emit back that to the client side. The interface uses promises due to handle asynchronous operation on the big JSON file which demands time and resource.

The express app uses SWIG template engine and serves static HTMLs from the view folder. These HTML files are organized on a layout manner, but the important part is the "mountPoint" id in the index.html. This id found by the bundled and served javascript file. This file has introduced through the entry.js which declare few globally available tools like underscore. My opinion is that the third parties should be separated from the written code base to avoid further conflicts during the version updates.

...
